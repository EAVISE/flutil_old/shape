from .shape import Box, Point, OrientedBox
from typing import List
import math
import numpy as np
from scipy.spatial import ConvexHull


def get_quadrant(angle):
    """Return in which quadrant a given angle lies.

    :param float angle: the angle in radians
    :returns: the quadrant of angle
    :rtype: int
    """
    pos_angle = angle % (2*np.pi)
    lower_bounds = [i*np.pi/2 for i in range(5)]
    return [bound < pos_angle for bound in lower_bounds].index(False)


def get_minimum_envelope_box(points: List[Point],
                             angle_guess: float=None) -> Box:
    """Return an OrientedBox object that represents the smallest bounding box
    for a given set of points.

    :param list points: a list of Point objects around which the smallest
    bounding box needs to be found
    :param float angle_guess: an estimate of the counter cockwise angle (in
    radians) of the bounding box; the difference with the angle found will be
    less than pi/2 radians
    :returns: an OrientedBox object representing the smallest bounding box,
    None for less than 3 points
    :rtype: OrientedBox, None
    """
    if len(points) < 3:
        return None
    pi2 = np.pi/2
    points = np.asarray([[p.x, p.y] for p in points])

    # Get convex hull for the points
    hull_points = points[ConvexHull(points).vertices]

    # calculate edge angles
    edges = np.zeros((len(hull_points)-1, 2))
    edges = hull_points[1:] - hull_points[:-1]

    angles = np.zeros((len(edges)))
    angles = np.arctan2(edges[:, 1], edges[:, 0])

    angles = np.abs(np.mod(angles, pi2))

    # find rotation matrices
    rotations = np.vstack([
        np.cos(angles),
        -np.sin(angles),
        np.sin(angles),
        np.cos(angles)]).T
    rotations = rotations.reshape((-1, 2, 2))

    # apply rotations to the hull
    rot_points = np.dot(rotations, hull_points.T)

    # find the bounding points
    min_x = np.nanmin(rot_points[:, 0], axis=1)
    max_x = np.nanmax(rot_points[:, 0], axis=1)
    min_y = np.nanmin(rot_points[:, 1], axis=1)
    max_y = np.nanmax(rot_points[:, 1], axis=1)

    # find the box with the smallest area
    areas = (max_x - min_x) * (max_y - min_y)
    best_index = np.argmin(areas)

    # return the smallest box
    x1 = max_x[best_index]
    x2 = min_x[best_index]
    y1 = max_y[best_index]
    y2 = min_y[best_index]
    r = rotations[best_index]

    angle = angles[best_index]
    width = abs(x1 - x2)
    height = abs(y1 - y2)
    center = np.dot([(x1 + x2)/2, (y1 + y2)/2], r)

    if angle_guess:
        # Get the alternative angle closest to the guess
        angle_alts = [(angle + i*np.pi/2) for i in range(-2, 3, 1)]
        closest_alt = np.argmin([abs(alt - angle_guess) for alt in angle_alts])

        new_angle = angle_alts[closest_alt]

        # Change width and height accordingly
        if (get_quadrant(angle) - get_quadrant(new_angle)) % 2 != 0:
            temp = width
            width = height
            height = temp
        angle = new_angle

    return OrientedBox(center[0], center[1], width, height, angle)


def eucledian_distance(point1: Point, point2: Point) -> float:
    """Return the eucledian distance between two points.

    :param Point point1: the first point
    :param Point point2: the other point
    :returns: the distance between point1 and point2
    :raises AttributeError: if point1 or point2 or not of type Point
    """
    return math.sqrt((point1.x - point2.x)**2 +
                     (point1.y - point2.y)**2)


def get_distance(point1: Point, point2: Point,
                 measure=eucledian_distance) -> float:
    """Return the distance between the two points using the given distance measure.

    :param Point point1: the first point
    :param Point point2: the other point
    :param measure: the measure to use (default: eucledian_distance)
    :returns: the distance between point1 and point2
    :raises AttributeError: if point1 or point2 or not of type Point
    """
    return measure(point1, point2)


def get_horizontal_slope(point1: Point, point2: Point) -> float:
    """Return the horizontal slope of the line that connects two points.

    :param Point point1: the first point
    :param Point point2: the other point
    :returns: the horizontal slope between the line that connects point1 and
    point2. Could be infinity.
    :raises AttributeError: if point1 or point2 or not of type Point
    """
    if point2.x != point1.x:
        return (point2.y - point1.y)/(point2.x - point1.x)
    else:
        # Vertical line
        return float('inf')


def get_horizontal_angle(point1: Point, point2: Point) -> float:
    """Return the horizontal angle in radians of the line that connects two points.

    :param Point point1: a point on the positive end of the line
    :param Point point2: a point on the negative end of the line
    :returns: the angle in radians between the line that connects point1 and
    point2 and the horizontal axis
    """
    computed_angle = math.atan(get_horizontal_slope(point1, point2))
    # Check in which quadrant the answer should lie
    if point1.x >= point2.x and point1.y >= point2.y:
        # First quadrant
        return computed_angle
    elif point1.x < point2.x and point1.y > point2.y:
        # Second quadrant: correct computed angle from 4th to 2nd quadrant
        return computed_angle + math.pi
    elif point1.x < point2.x and point1.y < point2.y:
        # Third quadrant: correct computed angle from 1st to 3rd quadrant
        return computed_angle + math.pi
    else:
        # Fourth quadrant
        return computed_angle


def get_vertical_angle(point1: Point, point2: Point) -> float:
    """Return the vertical angle in radians of the line that connects two points.

    :param Point point1: the first point
    :param Point point2: the other point
    :returns: the angle in radians between the line that connects point1 and
    point2 and the vertical axis
    """
    return get_horizontal_angle(point1, point2) - (math.pi / 2)


def basis_transform(old_basis, old_origin, vector):
    """Transform a vector defined in an old basis to our coordinate system and
    return the new, transformed vector.

    :param tuple old_basis: a tuple of 2 points describing the old basis in our
    coordinate system :param Point old_origin: the origin of the old basis
    described in our coordinate system :param Point vector: the vector
    described in the old coordinate system :returns: the vector described in
    our coordinate system
    :rtype: Point
    """
    s_11 = old_basis[0].x
    s_21 = old_basis[0].y
    s_12 = old_basis[1].x
    s_22 = old_basis[1].y
    x = s_11 * vector.x + s_12 * vector.y + old_origin.x
    y = s_21 * vector.x + s_22 * vector.y + old_origin.y
    return Point(x, y)


def get_size_of_rotated_rectangle_bounds(rect_size, angle):
    """Return the size (width, height) of the bounding box of a rotated rectangle.

    :param tuple rect_size: (width, height) of the rectangle
    :param float angle: oriented angle (in randians) of the rectangle
    :returns: the size (width, height) of the bounding box of the rotated
    rectangle
    :rtype: tuple
    """
    new_width = rect_size[1]*math.sin(angle) + rect_size[0]*math.cos(angle)
    new_height = rect_size[1]*math.cos(angle) + rect_size[0]*math.sin(angle)
    return (new_width, new_height)


def basis_transform_angle(angle, old_origin, vector):
    """Transform a vector defined in an old orthonormal basis (that is at a
    given angle to our basis) to our coordinate system and return the new,
    transformed vector.

    :param float angle: the oriented angle in radians that the old basis makes
    with our basis. Measured from our coordinate system to theirs, and being
    positive if the oriented angle is clockwise.
    :param Point old_origin: the origin of the old basis described in our
    coordinate system
    :param Point vector: the vector described in the old coordinate system
    :returns: the vector described in our coordinate system
    :rtype: Point
    """
    old_basis = (Point(math.cos(angle), -math.sin(angle)),
                 Point(math.sin(angle), math.cos(angle)))
    return basis_transform(old_basis, old_origin, vector)


def get_upper_left_corner(oriented_box):
    """Return the upper left corner of an oriented box.

    The coordinate system in which the center of the box is defined, is assumed
    to be left handed (positive x to right, positive y down).

    :param OrientedBox oriented_box: the box
    :returns: the point in the upper left hand corner of the box
    :rtype: Point, None
    """
    if not oriented_box:
        return None
    angle = oriented_box.angle
    width = oriented_box.width
    height = oriented_box.height

    old_origin = Point(oriented_box.x, oriented_box.y)
    vector = Point(-width/2, -height/2)
    return basis_transform_angle(angle, old_origin, vector)


def get_upper_right_corner(oriented_box):
    """Return the upper right corner of an oriented box.

    The coordinate system in which the center of the box is defined, is assumed
    to be right handed (positive x to right, positive y down).

    :param OrientedBox oriented_box: the box
    :returns: the point in the upper right hand corner of the box
    :rtype: Point, None
    """
    if not oriented_box:
        return None
    angle = oriented_box.angle
    width = oriented_box.width
    height = oriented_box.height

    old_origin = Point(oriented_box.x, oriented_box.y)
    vector = Point(width/2, -height/2)
    return basis_transform_angle(angle, old_origin, vector)


def get_lower_left_corner(oriented_box):
    """Return the lower left corner of an oriented box.

    The coordinate system in which the center of the box is defined, is assumed
    to be right handed (positive x to right, positive y down).

    :param OrientedBox oriented_box: the box
    :returns: the point in the lower left hand corner of the box
    :rtype: Point, None
    """
    if not oriented_box:
        return None
    angle = oriented_box.angle
    width = oriented_box.width
    height = oriented_box.height

    old_origin = Point(oriented_box.x, oriented_box.y)
    vector = Point(-width/2, height/2)
    return basis_transform_angle(angle, old_origin, vector)


def get_lower_right_corner(oriented_box):
    """Return the lower right corner of an oriented box.

    The coordinate system in which the center of the box is defined, is assumed
    to be right handed (positive x to right, positive y down).

    :param OrientedBox oriented_box: the box
    :returns: the point in the lower right hand corner of the box
    :rtype: Point, None
    """
    if not oriented_box:
        return None
    angle = oriented_box.angle
    width = oriented_box.width
    height = oriented_box.height

    old_origin = Point(oriented_box.x, oriented_box.y)
    vector = Point(width/2, height/2)
    return basis_transform_angle(angle, old_origin, vector)


def get_bottom_center(oriented_box):
    """Return the bottom center of an oriented box.

    The coordinate system in which the center of the box is defined, is assumed
    to be left handed (positive x to right, positive y down).

    :param OrientedBox oriented_box: the box
    :returns: the point in the bottom center of the box
    :rtype: Point, None
    """
    if not oriented_box:
        return None
    angle = oriented_box.angle
    height = oriented_box.height

    old_origin = Point(oriented_box.x, oriented_box.y)
    vector = Point(0, height/2)
    return basis_transform_angle(angle, old_origin, vector)


def get_top_center(oriented_box):
    """Return the top center of an oriented box.

    The coordinate system in which the center of the box is defined, is assumed
    to be left handed (positive x to right, positive y down).

    :param OrientedBox oriented_box: the box
    :returns: the point in the top center of the box
    :rtype: Point, None
    """
    if not oriented_box:
        return None
    angle = oriented_box.angle
    height = oriented_box.height

    old_origin = Point(oriented_box.x, oriented_box.y)
    vector = Point(0, -height/2)
    return basis_transform_angle(angle, old_origin, vector)
