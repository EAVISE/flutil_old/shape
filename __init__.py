from collections import namedtuple
from matplotlib.patches import Rectangle
import copy


class Point:
    """A point in space.

    :param float x: the x-coordinate of the point
    :param float y: the y-coordinate of the point
    """
    def __init__(self,
                 x: float,
                 y: float):
        self.x = x
        self.y = y

    def __repr__(self):
        return f'{self.x, self.y}'

    def __iadd__(self, other):
        if isinstance(other, Point):
            self.x += other.x
            self.y += other.y
        elif isinstance(other, tuple):
            self.x += other[0]
            self.y += other[1]
        else:
            self.x += other
            self.y += other
        return self

    def __isub__(self, other):
        if isinstance(other, Point):
            self.x -= other.x
            self.y -= other.y
        elif isinstance(other, tuple):
            self.x -= other[0]
            self.y -= other[1]
        else:
            self.x -= other
            self.y -= other
        return self

    def __imul__(self, other):
        if isinstance(other, tuple):
            self.x *= other[0]
            self.y *= other[1]
        elif isinstance(other, Point):
            self.x *= other.x
            self.y *= other.y
        else:
            self.x *= other
            self.y *= other
        return self

    def __itruediv__(self, other):
        if isinstance(other, tuple):
            self.x /= other[0]
            self.y /= other[1]
        elif isinstance(other, Point):
            self.x /= other.x
            self.y /= other.y
        else:
            self.x /= other
            self.y /= other
        return self

    def __add__(self, other):
        cp = copy.deepcopy(self)
        cp += other
        return cp

    def __sub__(self, other):
        cp = copy.deepcopy(self)
        cp -= other
        return cp

    def __mul__(self, other):
        cp = copy.deepcopy(self)
        cp *= other
        return cp

    def __truediv__(self, other):
        cp = copy.deepcopy(self)
        cp /= other
        return cp

    def __eq__(self, other):
        if not isinstance(other, Point):
            return False
        else:
            return (self.x == other.x
                    and self.y == other.y)

    def __iter__(self):
        return iter([self.x, self.y])


class Box:
    """An easy-to-use box.

    :param float x_min: the x-coordinate of the left side of the rectangle
    :param float y_min: the y-coordinate of the top side of the rectangle
    :param float x_max: the x-coordinate of the right side of the rectangle
    :param float y_max: the y-coordinate of the bottom side of the rectangle
    """
    def __init__(self,
                 x_min: float,
                 y_min: float,
                 x_max: float,
                 y_max: float):
        self._x_min = x_min
        self._y_min = y_min
        self._x_max = x_max
        self._y_max = y_max

    def __repr__(self):
        return f'{self.x_min, self.y_min, self.x_max, self.y_max}'

    def __iter__(self):
        return iter(self.to_tuple())

    def __iadd__(self, other):
        if isinstance(other, tuple):
            self.center = tuple([c + o for c, o in zip(self.center, other)])
        else:
            self.center += other
        return self

    def __isub__(self, other):
        if isinstance(other, tuple) or isinstance(other, Point):
            self.center += tuple(-o for o in other)
        else:
            self.center += -other
        return self

    def __imul__(self, other):
        """Rescale the box with factor `b`"""
        if isinstance(other, tuple):
            factor = other
        else:
            factor = (other, other)
        self.width *= factor[0]
        self.height *= factor[1]
        return self

    def __itruediv__(self, b):
        """Rescale the box with factor `1/b`"""
        if isinstance(b, tuple):
            self *= tuple(1/x for x in b)
        else:
            self *= 1/b
        return self

    def __add__(self, other):
        cp = copy.deepcopy(self)
        cp += other
        return cp

    def __sub__(self, other):
        cp = copy.deepcopy(self)
        cp -= other
        return cp

    def __mul__(self, other):
        cp = copy.deepcopy(self)
        cp *= other
        return cp

    def __truediv__(self, other):
        cp = copy.deepcopy(self)
        cp /= other
        return cp

    def __and__(self, other):
        """The area of overlap of two boxes."""
        dx = min(self.x_max, other.x_max) - max(self.x_min, other.x_min)
        dy = min(self.y_max, other.y_max) - max(self.y_min, other.y_min)
        if dx >= 0 and dy >= 0:
            return dx * dy
        else:
            return 0

    def __or__(self, other):
        """The union area of two boxes."""
        overlap = self & other
        return self.area + other.area - overlap

    def __contains__(self, other):
        """Check if `other` is inside the `self` Box.

        `other` can be a Box, Point or tuple (x, y) instance.
        """
        if isinstance(other, Point):
            return (other.x >= self.x_min
                    and other.y >= self.y_min
                    and other.x <= self.x_max
                    and other.y <= self.y_max)
        elif isinstance(other, tuple):
            return (other[0] >= self.x_min
                    and other[1] >= self.y_min
                    and other[0] <= self.x_max
                    and other[1] <= self.y_max)
        elif isinstance(other, Box):
            return (other.x_min >= self.x_min
                    and other.y_min >= self.y_min
                    and other.x_max <= self.x_max
                    and other.y_max <= self.y_max)

    def __eq__(self, other):
        if not isinstance(other, Box):
            return False
        return (self.x_min == other.x_min
                and self.y_min == other.y_min
                and self.x_max == other.x_max
                and self.y_max == other.y_max)

    def __lt__(self, other):
        if not isinstance(other, Box):
            return False
        return self.area < other.area

    def __gt__(self, other):
        if not isinstance(other, Box):
            return False
        return self.area > other.area

    def __hash__(self):
        return hash(tuple(self))

    @staticmethod
    def from_width_height(width: float, height: float, center: Point=None):
        """Return a new ``Box`` from a width and a height.

        :param float width: the width of the new ``Box``
        :param float height: the height of the new ``Box``
        :param Point center: Optional, the center cooredinate of the box. If
        ``None``, the center will be put in the middle of ``width`` and
        ``height`` (i.e. the ``Box`` will have top left coordinates (0, 0) ).
        """
        if center is None:
            center = Point(x=width/2, y=height/2)

        x_min = center.x - width/2
        y_min = center.y - height/2
        x_max = center.x + width/2
        y_max = center.y + height/2

        return Box(x_min=x_min, y_min=y_min,
                   x_max=x_max, y_max=y_max)

    @property
    def x_min(self):
        return self._x_min

    @x_min.setter
    def x_min(self, value):
        self._x_min = value

    @property
    def y_min(self):
        return self._y_min

    @y_min.setter
    def y_min(self, value):
        self._y_min = value

    @property
    def x_max(self):
        return self._x_max

    @x_max.setter
    def x_max(self, value):
        self._x_max = value

    @property
    def y_max(self):
        return self._y_max

    @y_max.setter
    def y_max(self, value):
        self._y_max = value

    @property
    def left(self):
        """The x-coordinate of the left boundary of the box."""
        return self.x_min

    @property
    def top(self):
        """The y-coordinate of the top boundary of the box."""
        return self.y_min

    @property
    def right(self):
        """The x-coordinate of the right boundary of the box."""
        return self.x_max

    @property
    def bottom(self):
        """The y-coordinate of the bottom boundary of the box."""
        return self.y_max

    @property
    def width(self):
        """The width of the box."""
        return self.x_max - self.x_min

    @width.setter
    def width(self, value):
        fac = value / self.width

        center_x = self.center.x
        new_x_min = int((self.x_min - center_x)*fac + center_x)
        new_x_max = int((self.x_max - center_x)*fac + center_x)

        self.x_min = new_x_min
        self.x_max = new_x_max

    @property
    def height(self):
        """The height of the box."""
        return self.y_max - self.y_min

    @height.setter
    def height(self, value):
        fac = value / self.height

        center_y = self.center.y
        new_y_min = int((self.y_min - center_y)*fac + center_y)
        new_y_max = int((self.y_max - center_y)*fac + center_y)

        self.y_min = new_y_min
        self.y_max = new_y_max

    @property
    def area(self):
        return self.width * self.height

    @property
    def bottom_left(self):
        """The bottom left coordinates of the box."""
        return (self.left, self.bottom)

    @property
    def bottom_right(self):
        """The bottom right coordinates of the box."""
        return (self.right, self.bottom)

    @property
    def top_left(self):
        """The top left coordinates of the box."""
        return (self.left, self.top)

    @property
    def top_right(self):
        """The top right coordinates of the box."""
        return (self.right, self.top)

    @property
    def center(self) -> Point:
        """The center coordinates of the box."""
        x_center = self.x_min + (self.x_max - self.x_min)/2
        y_center = self.y_min + (self.y_max - self.y_min)/2
        return Point(x_center, y_center)

    @center.setter
    def center(self, value):
        width = self.width
        height = self.height
        if isinstance(value, Point):
            self.x_min = value.x - width/2
            self.y_min = value.y - height/2
            self.x_max = value.x + width/2
            self.y_max = value.y + height/2
        elif isinstance(value, tuple):
            self.x_min = value[0] - width/2
            self.y_min = value[1] - height/2
            self.x_max = value[0] + width/2
            self.y_max = value[1] + height/2
        else:
            raise NotImplementedError('Setter not implemented for type '
                                      f'{type(value)}.')

    def iou(self, other):
        """Return the intersection over union (IOU) of the two boxes."""
        return (self & other)/(self | other)

    def to_int(self):
        """Return copy with all the values converted to an int."""
        return Box(x_min=int(self.x_min),
                   y_min=int(self.y_min),
                   x_max=int(self.x_max),
                   y_max=int(self.y_max))

    def coordinate_from_char(self, coor_char: str):
        """Return the box's coordinate that corresponds to the given char.

        :param str coor_char: character describing the desired coordinate. 't'
        for top, 'r' for right, 'b' for bottom and 'l' for left.
        """
        if coor_char == 't':
            return self.top
        elif coor_char == 'r':
            return self.right
        elif coor_char == 'b':
            return self.bottom
        elif coor_char == 'l':
            return self.left

    def to_tuple(self, order: str='ltrb'):
        """Return the box as a tuple with its coordinates in certain order.

        :param str order: Optional, string describing the desired order of the
        tuple.  't' for top, 'r' for right, 'b' for bottom and 'l' for left.
        E.g.: 'trbl' will return a (top, right, bottom, left)-tuple, while
        'ltrb' will return a (left, top, right, bottom)-tuple. Default returns
        tuple with order ``x_min``, ``y_min``, ``x_max``, ``y_max``.
        """
        coordinates = []
        for char in order:
            coordinates.append(self.coordinate_from_char(char))
        return tuple(coordinates)

    def css(self):
        """Return the box as a (top, right, bottom, left)-tuple."""
        return self.to_tuple('trbl')

    def to_patch(self, *args, **kwargs):
        """Return the box as a matplotlib ``Patch``."""
        if 'fill' in kwargs:
            fill = kwargs['fill']
            del kwargs['fill']
        else:
            fill = False
        return Rectangle((self.left, self.top),
                         self.width,
                         self.height,
                         *args,
                         fill=fill,
                         **kwargs)

    def scale_xy(self, x_factor, y_factor,
                 min_x=-float('inf'), min_y=-float('inf'),
                 max_x=float('inf'), max_y=float('inf')):
        """Return an x- and y-scaled variant of this ``Box``.

        :param float x_factor: the factor to scale with in the x-direction
        :param float y_factor: the factor to scale with in the y-direction
        :param float min_x: the smallest allowed x-value, the box will clip to
        this value
        :param float min_y: the smallest allowed y-value, the box will clip to
        this value
        :param float max_x: the largest allowed x-value, the box will clip to
        this value
        :param float max_y: the largest allowed y-value, the box will clip to
        this value
        """
        scaled_box = self * (x_factor, y_factor)
        scaled_box.x_min = max(min_x, scaled_box.x_min)
        scaled_box.y_min = max(min_y, scaled_box.y_min)
        scaled_box.x_max = min(max_x, scaled_box.x_max)
        scaled_box.y_max = min(max_y, scaled_box.y_max)

        return scaled_box

    def scale(self, factor,
              min_x=-float('inf'), min_y=-float('inf'),
              max_x=float('inf'), max_y=float('inf')):
        """Return a uniformly scaled variant of this ``Box``.

        :param float factor: the factor to scale with
        :param float min_x: the smallest allowed x-value, the box will clip to
        this value
        :param float min_y: the smallest allowed y-value, the box will clip to
        this value
        :param float max_x: the largest allowed x-value, the box will clip to
        this value
        :param float max_y: the largest allowed y-value, the box will clip to
        this value
        """
        return self.scale_xy(x_factor=factor, y_factor=factor,
                             min_x=min_x, min_y=min_y,
                             max_x=max_x, max_y=max_y)

    def shift(self, delta_x, delta_y,
              min_x=-float('inf'), min_y=-float('inf'),
              max_x=float('inf'), max_y=float('inf')):
        """Return a shifted version of this ``Box``.

        :param float delta_x: the horizontal shift
        :param float delta_y: the vertical shift
        :param float min_x: the smallest allowed x-value, the box will clip to
        this value
        :param float min_y: the smallest allowed y-value, the box will clip to
        this value
        :param float max_x: the largest allowed x-value, the box will clip to
        this value
        :param float max_y: the largest allowed y-value, the box will clip to
        this value
        """
        shifted_box = self + (delta_x, delta_y)
        shifted_box.x_min = max(min_x, shifted_box.x_min)
        shifted_box.y_min = max(min_y, shifted_box.y_min)
        shifted_box.x_max = min(max_x, shifted_box.x_max)
        shifted_box.y_max = min(max_y, shifted_box.y_max)
        return shifted_box


_OrientedBox = namedtuple('OrientedBox', 'x, y, width, height, angle')


class OrientedBox(_OrientedBox):
    """A more general representation of a Box.

    :param x: the x-coordinate of the center point
    :param y: the y-coordinate of the center point
    :param width: the width of the rectangle
    :param height: the height of the rectangle
    :param angle: the angle in radians that the base makes with the x-axis
    """
    def __sub__(self, other):
        if isinstance(other, OrientedBox):
            return self.x - other.x +\
                self.y - other.y +\
                self.width - other.width +\
                self.height - other.height +\
                self.angle - other.angle
        else:
            return NotImplemented


class Shape:
    """A general representation of a geometric shape.

    The Shape is represented by a list of Point objects. The Shape object is
    iterable where an iterator will simply iterate through the points.
    """
    def __init__(self, points):
        """Initialize a Shape object.

        :param list points: a list of Point objects representing
        the geometric shape. The order of the points is of importance
        as it represents in which order lines should be drawn to create
        an enclosed shape.
        """
        self.points = points

    @property
    def envelope(self):
        """Get a Box that envelopes a list of Points."""
        min_x_point = min(map(lambda p: p.x, self.points))
        max_x_point = max(map(lambda p: p.x, self.points))
        min_y_point = min(map(lambda p: p.y, self.points))
        max_y_point = max(map(lambda p: p.y, self.points))
        return Box(min_x_point, min_y_point, max_x_point, max_y_point)

    @property
    def width(self):
        return self.x_max - self.x_min

    @property
    def height(self):
        return self.y_max - self.y_min

    @property
    def x_min(self):
        return min(p.x for p in self.points)

    @x_min.setter
    def x_min(self, value):
        if self.width == 0:
            for p in self.points:
                p.x = value
        else:
            new_width = self.x_max - value
            stretch = (new_width / self.width, 1)
            self.points = [((p - (self.x_max, 0)) * stretch) + (self.x_max, 0)
                           for p in self.points]

    @property
    def y_min(self):
        return min(p.y for p in self.points)

    @y_min.setter
    def y_min(self, value):
        if self.height == 0:
            for p in self.points:
                p.y = value
        else:
            new_height = self.y_max - value
            stretch = (1, new_height / self.height)
            self.points = [((p - (0, self.y_max)) * stretch) + (0, self.y_max)
                           for p in self.points]

    @property
    def x_max(self):
        return max(p.x for p in self.points)

    @x_max.setter
    def x_max(self, value):
        if self.width == 0:
            for p in self.points:
                p.x = value
        else:
            new_width = value - self.x_min
            stretch = (new_width / self.width, 1)
            self.points = [((p - (self.x_min, 0)) * stretch) + (self.x_min, 0)
                           for p in self.points]

    @property
    def y_max(self):
        return max(p.y for p in self.points)

    @y_max.setter
    def y_max(self, value):
        if self.height == 0:
            for p in self.points:
                p.y = value
        else:
            new_height = value - self.y_min
            stretch = (1, new_height / self.height)
            self.points = [((p - (0, self.y_min)) * stretch) + (0, self.y_min)
                           for p in self.points]

    @property
    def center(self):
        """The center of the (bounding box of the) shape."""
        return self.envelope.center

    @center.setter
    def center(self, value):
        shift = value - self.center
        self.points = [p + shift for p in self.points]

    def __iadd__(self, other):
        self.points = [p + other for p in self.points]
        return self

    def __isub__(self, other):
        self.points = [p - other for p in self.points]
        return self

    def __imul__(self, other):
        self.points = [p * other for p in self.points]
        return self

    def __itruediv__(self, other):
        self.points = [p / other for p in self.points]
        return self

    def __add__(self, other):
        cp = copy.deepcopy(self)
        cp += other
        return cp

    def __sub__(self, other):
        cp = copy.deepcopy(self)
        cp -= other
        return cp

    def __mul__(self, other):
        cp = copy.deepcopy(self)
        cp *= other
        return cp

    def __truediv__(self, other):
        cp = copy.deepcopy(self)
        cp /= other
        return cp

    def __iter__(self):
        return iter(self.points)

    def __eq__(self, other):
        if not isinstance(other, Shape):
            return False
        return all(p1 == p2 for p1, p2 in zip(self, other))
