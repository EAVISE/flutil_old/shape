#!/usr/bin/env python3
import unittest
import arithmetic as ba
from arithmetic import Box, Point, OrientedBox
import math

class SimpleBoxTestCase(unittest.TestCase):
    def setUp(self):
        # Box with bottom left corner in origin
        self.bl_box = Box(0, 0, 1, 1)

        # Box with center in origin
        self.center_box = Box(-0.5, -0.5, 0.5, 0.5)

        # Point in origin
        self.origin = Point(0 ,0)

        # Distant point
        self.far_far_away = Point(100, 100)

    def test_get_area(self):
        self.assertEqual(ba.get_area(self.center_box), 1)
        self.assertEqual(ba.get_area(self.bl_box), 1)

    def test_get_area_of_overlap(self):
        self.assertEqual(ba.get_area_of_overlap(self.center_box, self.bl_box),
                         0.5*0.5)

    def test_get_relative_overlap(self):
        self.assertEqual(ba.get_relative_overlap(self.center_box, self.bl_box),
                         0.25)

    def test_is_box_in_box(self):
        tiny_center_box = Box(-0.1, -0.1, 0.1, 0.1)
        self.assertTrue(ba.is_box_in_box(tiny_center_box, self.center_box))
        self.assertFalse(ba.is_box_in_box(self.bl_box, self.center_box))

    def test_is_point_in_box(self):
        self.assertTrue(ba.is_point_in_box(self.origin, self.center_box))
        self.assertTrue(ba.is_point_in_box(self.origin, self.bl_box))

        self.assertFalse(ba.is_point_in_box(self.far_far_away, self.center_box))

    def test_are_points_in_box(self):
        # Trivial case
        self.assertFalse(ba.are_points_in_box([], self.center_box))
        # Upper right corner of the center box
        ur_corner = Point(0.5, 0.5)

        good_points = [self.origin, ur_corner]
        self.assertTrue(ba.are_points_in_box(good_points, self.center_box))

        bad_points = [self.origin, self.far_far_away]
        self.assertFalse(ba.are_points_in_box(bad_points, self.center_box))

    def test_get_envelope_box(self):
        envelope = Box(self.origin.x, self.origin.y, self.far_far_away.x, self.far_far_away.y)
        halfway_there = Point(50, 50)
        points = [self.origin, halfway_there, self.far_far_away]
        self.assertEqual(ba.get_envelope_box(points), envelope)

    def test_get_minimum_envelope_box(self):
        envelope = OrientedBox(self.origin.x, self.origin.y, 1.0, 1.0, math.pi / 4)
        top = Point(0, math.sqrt(2)/2)
        bottom = Point(0, -math.sqrt(2)/2)
        left = Point(-math.sqrt(2)/2, 0)
        right = Point(math.sqrt(2)/2, 0)
        points = [top, bottom, left, right]
        self.assertAlmostEqual(envelope, ba.get_minimum_envelope_box(points))

    def test_eucledian_distance(self):
        with self.assertRaises(AttributeError):
            ba.eucledian_distance(None, None)
        point1 = Point(0, 0)
        point2 = Point(1, 1)
        self.assertEqual(ba.eucledian_distance(point1, point2), math.sqrt(2))

    def test_get_distance(self):
        with self.assertRaises(AttributeError):
            ba.get_distance(None, None)
        point2 = Point(1, 1)
        self.assertEqual(ba.get_distance(self.origin, point2), math.sqrt(2))

    def test_get_horizontal_slope(self):
        point2 = Point(1, -1)
        with self.assertRaises(AttributeError):
            ba.get_horizontal_slope(None, None)
        self.assertEqual(ba.get_horizontal_slope(self.origin, point2), -1)

    def test_get_horizontal_angle(self):
        point2 = Point(0, 1)
        with self.assertRaises(AttributeError):
            ba.get_horizontal_angle(None, None)
        self.assertEqual(ba.get_horizontal_angle(self.origin, point2), math.pi / 2)

    def test_get_vertical_angle(self):
        point2 = Point(0, 1)
        with self.assertRaises(AttributeError):
            ba.get_vertical_angle(None, None)
        self.assertEqual(ba.get_vertical_angle(self.origin, point2), 0)

    def test_get_center_point(self):
        point2 = Point(1, 1)
        center = Point(0.5, 0.5)
        with self.assertRaises(AttributeError):
            ba.get_center_point(None, None)
        self.assertEqual(ba.get_center_point(self.origin, point2), center)

    def test_get_center_of_box(self):
        self.assertEqual(ba.get_center_of_box(self.center_box), self.origin)

    def test_get_upper_left_corner(self):
        center_x = math.sqrt(2)/2
        center_y = math.sqrt(2)/2
        width = 1
        height = 1
        angle = math.radians(45)
        oriented_box = OrientedBox(center_x, center_y, width, height, angle)

        # Correct upper left corner coordinates of the oriented box
        correct_upper_left_x = 0.0
        correct_upper_left_y = math.sqrt(2)/2

        # Calculated upper left corner
        calc_upper_left_x = ba.get_upper_left_corner(oriented_box).x
        calc_upper_left_y = ba.get_upper_left_corner(oriented_box).y

        # Assert
        self.assertAlmostEqual(calc_upper_left_x, correct_upper_left_x)
        self.assertAlmostEqual(calc_upper_left_y, correct_upper_left_y)

    def test_get_upper_right_corner(self):
        center_x = math.sqrt(2)/2
        center_y = math.sqrt(2)/2
        width = 1
        height = 1
        angle = math.radians(45)
        oriented_box = OrientedBox(center_x, center_y, width, height, angle)

        # Correct upper right corner coordinates of the oriented box
        correct_upper_right_x = math.sqrt(2)/2
        correct_upper_right_y = 0.0

        # Calculated upper right corner
        calc_upper_right_x = ba.get_upper_right_corner(oriented_box).x
        calc_upper_right_y = ba.get_upper_right_corner(oriented_box).y

        # Assert
        self.assertAlmostEqual(calc_upper_right_x, correct_upper_right_x)
        self.assertAlmostEqual(calc_upper_right_y, correct_upper_right_y)

    def test_get_lower_left_corner(self):
        center_x = math.sqrt(2)/2
        center_y = math.sqrt(2)/2
        width = 1
        height = 1
        angle = math.radians(45)
        oriented_box = OrientedBox(center_x, center_y, width, height, angle)

        # Correct lower left corner coordinates of the oriented box
        correct_lower_left_x = math.sqrt(2)/2
        correct_lower_left_y = math.sqrt(2)

        # Calculated lower left corner
        calc_lower_left_x = ba.get_lower_left_corner(oriented_box).x
        calc_lower_left_y = ba.get_lower_left_corner(oriented_box).y

        # Assert
        self.assertAlmostEqual(calc_lower_left_x, correct_lower_left_x)
        self.assertAlmostEqual(calc_lower_left_y, correct_lower_left_y)

    def test_get_lower_right_corner(self):
        center_x = math.sqrt(2)/2
        center_y = math.sqrt(2)/2
        width = 1
        height = 1
        angle = math.radians(45)
        oriented_box = OrientedBox(center_x, center_y, width, height, angle)

        # Correct lower right corner coordinates of the oriented box
        correct_lower_right_x = math.sqrt(2)
        correct_lower_right_y = math.sqrt(2)/2

        # Calculated lower right corner
        calc_lower_right_x = ba.get_lower_right_corner(oriented_box).x
        calc_lower_right_y = ba.get_lower_right_corner(oriented_box).y

        # Assert
        self.assertAlmostEqual(calc_lower_right_x, correct_lower_right_x)
        self.assertAlmostEqual(calc_lower_right_y, correct_lower_right_y)

    def test_get_bottom_center(self):
        center_x = math.sqrt(2)/2
        center_y = math.sqrt(2)/2
        width = 1
        height = 1
        angle = math.radians(45)
        oriented_box = OrientedBox(center_x, center_y, width, height, angle)

        # Correct bottom center coordinates of the oriented box
        correct_bottom_center_x = 3*math.sqrt(2)/4
        correct_bottom_center_y = 3*math.sqrt(2)/4

        # Calculated bottom center
        calc_bottom_center_x = ba.get_bottom_center(oriented_box).x
        calc_bottom_center_y = ba.get_bottom_center(oriented_box).y

        # Assert
        self.assertAlmostEqual(calc_bottom_center_x, correct_bottom_center_x)
        self.assertAlmostEqual(calc_bottom_center_y, correct_bottom_center_y)

    def test_get_top_center(self):
        center_x = math.sqrt(2)/2
        center_y = math.sqrt(2)/2
        width = 1
        height = 1
        angle = math.radians(45)
        oriented_box = OrientedBox(center_x, center_y, width, height, angle)

        # Correct top center coordinates of the oriented box
        correct_top_center_x = math.sqrt(2)/4
        correct_top_center_y = math.sqrt(2)/4

        # Calculated top center
        calc_top_center_x = ba.get_top_center(oriented_box).x
        calc_top_center_y = ba.get_top_center(oriented_box).y

        # Assert
        self.assertAlmostEqual(calc_top_center_x, correct_top_center_x)
        self.assertAlmostEqual(calc_top_center_y, correct_top_center_y)

    def test_get_quadrant(self):
        self.assertEqual(ba.get_quadrant(-math.pi/4), 4)

    def test_get_size_of_rotated_rectangle_bounds(self):
        size = (1.0, 1.0)
        angle = math.pi/4
        correct_bound_size = (math.sqrt(2), math.sqrt(2))
        self.assertAlmostEqual(ba.get_size_of_rotated_rectangle_bounds(size,
                                                                       angle)[0],
                               correct_bound_size[0])
        self.assertAlmostEqual(ba.get_size_of_rotated_rectangle_bounds(size,
                                                                       angle)[1],
                               correct_bound_size[1])

if __name__ == '__main__':
    unittest.main()
